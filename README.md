## Description

Permet de constituer des groupes d'élèves et d'attribuer un créneau horaire aux profs pour l'examen de TP de prépa.

Les élèves ont suivi les cours d'un ou 2 profs selon leurs options et ne doivent pas être surveillés par l'un d'eux lors de l'examen.
Chaque élève tire au sort un TP à passer. Les TP, symbolisés par une lettre en majuscule, sont planifiés dans différentes salles et créneaux horaires.

Pour tester rapidement l'application, lancer `client/index.html` dans un navigateur et cliquer sur le bouton `importer`.

## Fonctions

* Génération optimisée des groupes d'élèves
* Choix du prof surveillant parmis tout ceux disponibles pour chaque groupe d'élèves
* Glisser/déplacer les groupes pour les échanger avec signalement visuel des TP
* Afficher/masquer les élèves
* Signalement visuel des problèmes (prof sur 2 TP du même créneau horaire, pas assez de profs dans un créneau
* Récapitulatif de l'emploi du temps des profs (avec version imprimable)
* Récapitulatif pour les élèves triés par classe (avec version imprimable et exportation CSV)
* Synthèse complète des créneaux de TP (avec version imprimable)
* Synthèse complète par profs (avec version imprimable)
* Enregistrement de l'état courant dans un fichier `shacker.json`
* Chargement d'un état enregistré (il doit s'appeler `shacker.json` et être dans le dossier `client`)

## Mise en œuvre

1. Saisir les données des élèves et des TP en s'inspirant des fichiers `datas/eleves.csv` et `datas/tp.csv`
2. Importer les fichiers CSV dans la base SQLite `datas/datas.sqlite` (la vider d'abord)
3. Modifier le fichier `datas/main.py` pour vos besoins (saisie de profs, TP particuliers, nombre d'élèves par groupe...)
4. Générer le fichier `client/datas.json` des groupes d'élèves en lançant `datas/main.py`
5. Lancer `client/index.html` dans un navigateur

## License

WTFPL : http://www.wtfpl.net/