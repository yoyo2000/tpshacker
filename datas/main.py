#!/usr/bin/python3
# coding: utf-8

import sqlite3

def get_tp():
    TP = []
    c.execute("SELECT DISTINCT code_tp FROM eleves ORDER BY code_tp")
    for ligne in c:
        TP.append(ligne["code_tp"])
    TP2 = []
    c.execute("SELECT DISTINCT code_tp FROM tp ORDER BY code_tp")
    for ligne in c:
        TP2.append(ligne["code_tp"])
    if TP != TP2:
        raise Exception("Incohérence entre les TP des 2 fichiers : {0}".format(list(set(TP).symmetric_difference(set(TP2)))))
    print("{0} TP : {1}".format(len(TP),TP))
    return TP

def get_tp_par_creneau():
    creneaux = []
    c.execute("SELECT DISTINCT horaire FROM tp ORDER BY horaire")
    for ligne in c:
        creneaux.append(ligne["horaire"])
    dico = {}
    req = "SELECT code_tp, salle FROM tp WHERE horaire= ?"
    for cre in creneaux:
        dico[cre] = []
        c.execute(req, (cre,))
        for ligne in c:
            dico[cre].append(ligne["code_tp"]+"_"+ligne["salle"])
    #print("{0} Créneaux : {1}".format(len(dico)*6, dico))
    return dico

def get_profs():
    profs = ['Prof_1', 'Prof_2', 'Prof_3', 'Prof_4', 'Prof_5', 'Prof_6', 'Prof_7', 'Prof_8'] # FIXME
    #~ c.execute("SELECT DISTINCT prof FROM eleves union SELECT DISTINCT profspe FROM eleves where profspe <> null ORDER BY prof")
    #~ for ligne in c:
        #~ profs.append(ligne["prof"])
    print("{0} profs : {1}".format(len(profs),profs))
    return profs

def get_eleves_par_tp(tp=None, ordre=1):
    """ ordre donne l'ordre de classement des profs dans la requete sql (profspe après prof ou pas)
    """
    if tp is None:
        return None
    eleves = []
    if ordre == 1:
        req = "SELECT nom FROM eleves WHERE code_tp = ? ORDER BY prof ASC, profspe ASC"
    else:
        req = "SELECT nom FROM eleves WHERE code_tp = ? ORDER BY profspe ASC, prof ASC"
    c.execute(req, (tp,))
    for ligne in c:
        eleves.append(ligne["nom"])
    print("{0} ({2}) : {1}".format(tp, eleves, len(eleves)))
    return eleves

def get_groupes_par_tp(tp=None, cpt=[]):
    if tp is None:
        return None
    groupe = []    # groupe = [{eleves:[...,...],profs_non[...,...]}, {...}]
    eleves = eleves_par_tp[tp]
    req = "SELECT prof, profspe FROM eleves WHERE code_tp = ? and nom= ?"

    if tp == "Y" or tp == "C" or tp == "F":  # FIXME
        groupe_eleves = [eleves[i:i + 3] for i in range(0, len(eleves), 3)] # groupage par 3
    else:
        groupe_eleves = [eleves[i:i + 4] for i in range(0, len(eleves), 4)] # groupage par 4

    for g in groupe_eleves: # g = 1 groupe d'éleves
        profs_non = []
        for e in g: # e = 1 élève
            c.execute(req, (tp,e))
            row = c.fetchone()
            if row["prof"] is not None: profs_non.append(row["prof"])
            if row["profspe"] is not None: profs_non.append(row["profspe"])
        d={"TP": tp, "eleves":g, "prof_non": sorted(list(set(profs_non))), "id": "grp-{num:02d}".format(num=cpt[0])}
        groupe.append(d)
        cpt[0] += 1
    print(groupe)
    return groupe


if __name__ == "__main__":
    conn = sqlite3.connect("datas.sqlite")          # ouverture de la base
    conn.row_factory = sqlite3.Row                  # accès facile aux colonnes
    c = conn.cursor()                               # obtention d'un curseur

    tp = get_tp()
    profs = get_profs()
    creneaux = get_tp_par_creneau()

    eleves_par_tp = {}
    for un_tp in tp:
        eleves_par_tp[un_tp] = get_eleves_par_tp(un_tp)

    groupes_par_tp = []
    cpt = [1]
    for un_tp in tp:
        groupes_par_tp += get_groupes_par_tp(un_tp, cpt)

    print("{0} groupes".format(len(groupes_par_tp)))
    print("======================")
    print(groupes_par_tp)

    dico = {"groupes": groupes_par_tp, "profs": profs, "TP": creneaux}

    import io, json
    with io.open('../client/datas.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(dico, ensure_ascii=False))

    c.close()
