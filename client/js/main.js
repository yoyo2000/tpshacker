// https://www.google.fr/search?source=hp&q=File%20API%20de%20%22HTML%205%22&meta=&aq=f&aqi=g10&aql=&oq=&gs_rfai=
  'use strict';
  var   profs = {},
      creneaux = {},
      groupes = {},
      shaker = {},  // Données liées au déplacement des groupes et au choix du prof
      eleves_par_classe = {};

  // StartsWith
  String.prototype.startsWith = function (str) {
    return (this.match("^" + str) === str);
  };

  // Suppression d'un élément de tableau par sa valeur
  Array.prototype.unset = function(val){
    var index = this.indexOf(val);
    if (index > -1) {
      this.splice(index,1);
    }
  };

  // Gestion du storage
  Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
  };
   
  Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
  };

  function creer_id(chaine){
    // Remplace les espaces, les points et les slashs par des "_" en vue de créer un id
    return chaine.replace(/[ ]/g, "_").replace(/[.]/g, "_").replace(/[/]/g, "_");
  }

  $(document).ready(function() {
    var AFF_ELEVE = '<span class="fa fa-users fa-fw"></span>&nbsp;  Afficher les élèves';
    var MASQ_ELEVE = '<span class="fa fa-users text-warning"></span>&nbsp;  Masquer les élèves';

    $("#btn-eleves").html(AFF_ELEVE);

    // Message de confirmation lorsqu'on quitte la page
    $(window).bind('beforeunload', function(){
      return "Avez-vous pensé à enregistrer ?";
    });
    
    // Sous chrome pour éviter un pb de sécurité, lancer avec "chrome.exe --allow-file-access-from-files"
    $.ajaxSetup({async:false});
    // Chargement des données json
    $.getJSON("datas.json", function(data) {
      if ($.isPlainObject(data)){
        profs = data.profs;
        creneaux = data.TP;
        groupes = data.groupes;
        affiche_profs();
        affiche_creneaux();
        affiche_groupes();
        affiche_eleves();
        
        // Initialisation du shaker
        $(".droppable").each(function(i,elem){
          shaker[$(this).attr("id")] = {"groupe": "", "prof": ""};
        });
        
        distribue_groupes();
      }else{
        alert( "Problème de chargement des données" );
      }
    })
    .fail(function( jqxhr, textStatus, error ) {
      alert( "Erreur : " + textStatus + ", " + error );
    });
    $.ajaxSetup({async:true});
    
    $(".draggable").each(function(i,elem){
      // Ajout du scope pour que seul les drop du tp A acceptent les drag du tp A, etc...
      $(this).draggable({scope:$(this).data("tp")});
    });
    
    $(".droppable").each(function(i,elem){
      // Ajout du scope pour que seul les drop du tp A acceptent les drag du tp A, etc...
      $(this).droppable({scope:$(this).data("tp")});
    });
    
    $('.draggable').draggable({scroll: true, revert:"invalid", handle: ".panel-heading", zIndex: 100, addClasses: false, opacity: 0.5,
      start: function() {
        var this_tp = $(this).draggable( "option", "scope" );
        // Coloration des drop correspondants au tp draggé
        $(".droppable").each(function(i,elem){
          if ($(elem).data("tp") == this_tp) $(elem).addClass("dropOK");
        });
      },
      stop: function() {
        // Retour à la coloration normale
        $(".droppable").removeClass("dropOK");
      }
    });
    $('.droppable').droppable({
      drop: function(ev, ui) {
        droppable_drop($(this), ui.draggable);
      }
    });
    
    $("select").on( "change", function() {
      // Lors du choix d'un prof dans une liste déroulante,
      // mise à jour dans le shaker.
      var id_groupe = $(this).attr("id").substring(0,$(this).attr("id").length-7); // retrouve l'id du groupe à partir de l'id du select ("grp-xx-select" => "grp-xx")
      mise_a_jour_prof(id_groupe, $(this).find("option:selected").attr("label"));
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      // Clic sur un des liens de tab
      if ($(this).attr("id") == "lien-recap"){
        affiche_recap_prof();
      }else if ($(this).attr("id") == "lien-eleves"){
        affiche_recap_eleves();
      }else if ($(this).attr("id") == "lien-imprimer"){
        affiche_imprimer();
      }else if ($(this).attr("id") == "lien-imprimer2"){
        affiche_imprimer2();
      }
    });

    $(document).on("click", "#btn-eleves", function(event){
      // Clic sur le bouton d'affichage/masquage des élèves
      if ($(this).hasClass("btn-info")){
        $(this).html(AFF_ELEVE);
        $(".tr-creneaux").attr("style","height:inherit;");
        $(".list-eleves").hide();
        redistribue_groupes(false);
      }else{
        $(this).html(MASQ_ELEVE);
        $(".tr-creneaux").attr("style","height:260px;");
        $(".list-eleves").show();
        redistribue_groupes(false);
      }
      $(this).toggleClass("btn-info");
    });

    // $(document).on("click", ".lien-select", function(event){
      // // Clic sur les liens "Sélectionner" dans le tableau d'aide des profs
      // // Permet de sélectionner le prof dans tous les groupes du TP choisi et met à jour le shaker en conséquence
      // var prof = $(this).parent().data("prof");
      // var tp = $(this).parent().data("tp");
      // $('.draggable[data-tp="'+tp+'"]').find("select option[label='"+ prof +"']").attr('selected','selected').each(function(e){
        // mise_a_jour_prof($(this).parents(".draggable").attr("id"), $(this).parent().find("option:selected").attr("label"));
      // });
    // });

    $(document).on("click", "#btn-import", function(event){
      // Clic sur le bouton "Import"
      
      if (confirm("L'importation va écraser tous les changements effectués. Continuer ?")) {
        $.getJSON("shaker.json", function(data) {
          if ($.isPlainObject(data)){
            shaker = data;
            redistribue_groupes(true);
          }else{
            alert( "Problème de chargement des données" );
          }
        })
        .fail(function( jqxhr, textStatus, error ) {
          alert( "Impossible de charger le fichier shaker.json.\n\n(" + textStatus + ", " + error + ")");
        });
      }
    });

    $(document).on("click", "#btn-export", function(event){
      // Clic sur le bouton "Export"
      //window.open( "data:text/json;charset=utf-8," + escape(JSON.stringify(shaker)));
      
      // Solution de téléchargement de json permettant de donner un nom au fichier
      var a         = document.createElement('a');
      a.href        = 'data:attachment/json;charset=utf-8,' + JSON.stringify(shaker);
      a.target      = '_blank';
      a.download    = 'shaker.json';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    });

    $(document).on("click", ".yo-print", function(event){
      // Clic sur les boutons "Imprimer"
      // Chaque bouton a l'id de l'élément à imprimer dans data-idtoprint et un titre dans data-titre
      var $elementtoprint = $("#" + $(this).data("idtoprint"));
      var titre = $(this).data("titre");
      $elementtoprint.printThis({
        debug: false,
        importCSS: false,
        printContainer: false,
        loadCSS: "css/print.css",
        pageTitle: "TP-Shaker : "+titre,
        removeInline: false,
        printDelay: 333,
        header: null
      });
    });

    $(document).on("click", ".yo-export", function(event){
      // Clic sur les boutons "Exporter dans un fichier"
      // Chaque bouton a l'id de l'élément à imprimer dans data-export et un titre dans data-titre
      var idtoexport = "#" + $(this).data("export");
      var titre = $(this).data("titre");
      CSV.begin(idtoexport).download(titre + '.csv').go();
      
      
      // CSV.
        // begin([
         // ["TS1", "Hello","World"],
         // ["TS1", "Paul","Earth"],
         // ["TS1", "Marvin","Mars"],
         // ["TS1", "Spock","Vulcan"]
        // ]).
        // table("output",{header:1,caption:"My First html5csv program"}).
        // download('coucou.csv').
        // go();      
      
      
    });
  });

  function distribue_groupes(){
    // Distribution initiale et automatique des tp
    var list_drop = [];
    // Constitution de la liste des éléments droppable
    $(".droppable").each(function (i,e){list_drop.push($(this).attr("id"));});
    // Pour chaque éléments draggable, on regarde dans la liste le 1er droppable ayant le même tp
    // et on le balance dessus, ensuite on le retire de la liste et on passe au draggable suivant
    $(".draggable").each(function(i,elem){
      $.each(list_drop, function(j, ledrop){
        //if ($(elem).data("tp") == list_drop[j].charAt(list_drop[j].length-1)){
        if ($(elem).data("tp") == list_drop[j].split("__")[1].split("_")[0]){ // retrouve la lettre tp dans "1-Mardi_8h00__C_123"
          droppable_drop($("#"+ledrop), $(elem));
          list_drop.unset(ledrop);
          return false;
        }
      });
    });
  }

  function droppable_drop($le_drop, $le_drag) {
    // Appelé lorsqu'on veut dropper un draggable
    
    // Gestion du bon positionnement du drag dans le drop
    var drop_p = $le_drop.offset();
    var drag_p = $le_drag.offset();
    var left_end = drop_p.left - drag_p.left - 6;
    var top_end = drop_p.top - drag_p.top + 10;
    $le_drag.animate({
      top: '+=' + top_end,
      left: '+=' + left_end
    });
    
    var id_cre_drag = retrouve_creneau($le_drag.attr("id"));
    
    // Si on dépose le groupe dans le même créneau qu'à l'origine : rien à faire
    if ($le_drop.attr("id") == id_cre_drag){
      return;
    }
    
    // Ajout du déplacement dans le shaker avec échange éventuel si place déjà prise
    var sh_drop = shaker[$le_drop.attr("id")];
    if (sh_drop.groupe === ""){
      // Le créneau est vide : remplissage des données dans shaker
      // /!\ on passe aussi ici lors d'un échange (voir appel à droppable_drop dans le else)
      shaker[$le_drop.attr("id")].groupe = $le_drag.attr("id");
      shaker[$le_drop.attr("id")].prof = recup_prof($le_drag);
      // Vérifie la cohérence (pas 2 fois le même prof dans le même créneau)
      verifie_coherence_creneau($le_drop.attr("id"));
    }else{
      // Créneau déjà occupé donc échange de groupe
      // Récupération de l'id du groupe occupant le créneau cible
      var id_grp_ini = shaker[$le_drop.attr("id")].groupe;
      // Récupération de l'id du creneau d'origine du groupe déplacé
      // Vidage du créneau d'origine dans shaker
      shaker[id_cre_drag].groupe = "";
      shaker[id_cre_drag].prof = "";
      // Vidage du créneau cible dans shaker
      shaker[$le_drop.attr("id")].groupe = "";
      shaker[$le_drop.attr("id")].prof = "";
      // Déplacement du groupe occupant le créneau cible dans le créneau d'origine
      droppable_drop($("#"+id_cre_drag), $("#"+id_grp_ini));
      // Remplissage des données du nouveau groupe
      shaker[$le_drop.attr("id")].groupe = $le_drag.attr("id");
      shaker[$le_drop.attr("id")].prof = recup_prof($le_drag);
      // Vérifie la cohérence (pas 2 fois le même prof dans le même créneau)
      verifie_coherence_creneau($le_drop.attr("id"));
    }
  }

  function affiche_groupes(){
    // Création initiale des groupes à partir de la liste groupes
    var ret = '<!-- row --><div class="row groupes" style="width:100%;">';
    $.each(groupes, function(i, val){
      var g = '<!-- col --><div id="'+val.id+'" class="col-md-1 draggable" data-tp="'+val.TP+'">';
      g += '<!-- panel --><div class="panel panel-default"><div class="panel-heading text-center" style="padding-top: 0px;"><span class="lead">'+val.TP+'</span><span class="badge pull-right" style="margin-top: 4px;"></span>'+menu_prof(val.prof_non, val.id)+'</div>';
      g += '<ul class="list-group list-eleves" style="display: none;">';
      $.each(val.eleves, function(i, elev) {
        g += '<li class="list-group-item">'+elev+'</li>';
      });
      g += '</ul>';
      g += '</div><!-- panel -->';
      g += '</div><!-- col -->';
      ret += g;
    });
    ret += '</div><!-- row -->';
    $("#content").append(ret);
    
    // Ajout de la pastille avec le nombre de profs
    var prof_tot = profs.length;
    var limite_basse = prof_tot / 3;
    var limite_haute = 2*prof_tot / 3;
    $(".draggable").each(function(i, elem){
      var nb_prof = $(elem).find("select").children().length-1 ;
      var style_badge;
      if (nb_prof <= limite_basse){
        style_badge = "danger";
      }else if (nb_prof > limite_haute){
        style_badge = "success";
      }else{
        style_badge = "warning";
      }
      $(elem).find(".badge").html(nb_prof).addClass(style_badge);
    }); 
  }

  function menu_prof(lst_profs_interdits, id_groupe){
    // Création du menu déroulant des profs à partir des profs interdits
    // Appelé par affiche_groupes
    var ret = '<select id="' + id_groupe + '-select" class="form-control">';
    ret += '<option label="(vide)"></option>';
    $.each(profs, function(i, prf) {
      if ($.inArray(prf, lst_profs_interdits)===-1){
        ret += '<option label="' + prf + '">' + prf + '</option>';
      }
    });
    ret += '</select>';
    return ret;
  }

  function affiche_creneaux(){
    // Création initiale des créneaux à partir de la liste creneaux
    // Classement par ordre (les créneaux sont préfixés par "1-", "2-", etc...
    var cre = $.map(creneaux, function(value, key) {return key;}).sort();
    var ret = '<div class="well">';
    $.each(cre, function(i, val) {
      ret += '<div id="'+creer_id(val)+'" class="panel panel-primary panel-yo" data-nom="'+val.substr(2);
      ret += '"><div class="panel-heading panel-heading-yo">'+val.substr(2)+'</div>';
      ret += '<table class="table table-bordered"><tbody><tr class="tr-creneaux">';
      
      // création des créneaux avec listage pour créer un id unique même si 2 tp sont identiques
      var liste_id = [];
      $.each(creneaux[val], function(j, tp_salle){
        // les tp sont suivis de leur salle ("X_121")
        var lettre_tp = tp_salle.split("_")[0];
        var salle_tp = tp_salle.split("_")[1];
        var test_id = creer_id(val+'__'+tp_salle);
        ret += '<td id="'+test_id+'" data-tp="'+lettre_tp+'" data-salle="'+salle_tp+'" data-creneau="'+val+'" style="vertical-align:middle;" class="col-md-1 droppable">'+lettre_tp+'</td>';
        liste_id.push(test_id);
      });
      ret += '</tr></tbody></table>';
      ret += '</div>';
    });
    ret += '</div>';
    $("#content").append(ret);
  }

  function affiche_profs(){
    // Création du tableau récapitulatif des profs à partir de la liste profs
    var ret = '<div class="well">';
    ret += '<div id="recap-profs" class="panel panel-primary"><div class="panel-heading">Emploi du temps des profs<button id="btn-imprimer-profs" class="btn btn-warning btn-panel pull-right yo-print hidden-print" data-idtoprint="recap-profs" data-titre="Profs">Imprimer</button></div>';
    ret += '<table class="table table-bordered yo-striped yo-recap print-pointille">';
    var cre = $.map(creneaux, function(value, key) {return key;}).sort();
    ret += '<thead><tr><td style="font-size: inherit;" class="no-bottom-border"><span class="hidden-print">#</span></td>';
    $.each(cre, function(i, val) {
      ret += '<th>'+val.substr(2)+'</th>';
    });
    ret += '</tr></thead><tbody>';
    $.each(profs, function(i, prf){
      ret += '<tr><th>'+prf+'</th>';
      $.each(cre, function(j, val) {
        ret += '<td id="'+creer_id(prf+"__"+val)+'"></td>';
      });
      ret += '</tr>';
    });
    ret += '</tbody></table>';
    ret += '</div>';
    ret += '</div>';
    $("#recap").html(ret);
  }

  function affiche_eleves(){
    // Création du tableau récapitulatif des élèves à partir de la liste groupes
    // 1er parcours des élèves pour déduire les différentes classes possibles
    var liste_classes = [];
    $.each(groupes, function(i, val){
      $.each(val.eleves, function(i, elev) {
        var split_eleve = elev.split(/[()]+/).filter(function(e) { return e; });
        if (split_eleve.length > 1){
          // On considère que la classe est après le nom entre parenthèses => "Toto A. (TS1)"
          if ($.inArray(split_eleve[1], liste_classes) === -1){
            liste_classes.push(split_eleve[1]);
          }
        }
      });
    });
    liste_classes.sort();
    liste_classes.push("(Aucune classe)");
    // Initialisation
    $.each(liste_classes, function(i, val){
      eleves_par_classe[val] = {};
    });
    
    // Listage de tous les élèves triés par classe
    $.each(groupes, function(i, val){
      $.each(val.eleves, function(i, elev) {
        var split_eleve = elev.split(/[()]+/).filter(function(e) { return e; });
        if (split_eleve.length > 1){
          // On considère que la classe est après le nom entre parenthèses => "Toto A. (TS1)"
          eleves_par_classe[split_eleve[1]][split_eleve[0]] = val.id;
        }else{
          eleves_par_classe["(Aucune classe)"][split_eleve[0]] = val.id;
        }
      });
    });
    // Construction du tableau
    var ret = '<div class="well">';
    $.each(Object.keys(eleves_par_classe), function(i,classe){
      if (Object.keys(eleves_par_classe[classe]).length > 0){
        ret += '<div id="recap-eleves-'+classe+'" class="panel panel-primary panel-yo-petit">';
        ret += '<div class="panel-heading">'+classe+'<button id="btn-imprimer-eleves" class="btn btn-warning btn-panel pull-right yo-print hidden-print" data-idtoprint="recap-eleves-'+classe+'" data-titre="Récapitulatif par classe">Imprimer</button>';
        ret += '<button id="btn-exporter-eleves" class="btn btn-success btn-panel pull-right yo-export" data-export="recap-eleves-'+classe+'" data-titre="'+classe+'" title="Exporter dans un fichier" style="margin-right: 10px;"><i class="fa fa-table"></i></button>';
        // ret += '<button id="btn-tout-exporter-eleves" class="btn btn-success btn-panel pull-right yo-export" data-export="eleves" data-titre="Élèves" title="Exporter dans un fichier" style="margin-right: 10px;"><i class="fa fa-table">Tout exporter</i></button>';
        ret += '</div>';
        ret += '<table class="table table-bordered yo-striped yo-recap print-pointille">';
        ret += '<thead><tr>';
        ret += '<th>Nom</th><th>Horaire</th><th>TP</th>';
        ret += '</tr></thead><tbody>';
        $.each(Object.keys(eleves_par_classe[classe]).sort(), function(i, eleve) {
          ret += '<th>'+eleve+'</th>';
          ret += '<td id="'+creer_id(eleve+"__cre")+'"></td>';
          ret += '<td id="'+creer_id(eleve+"__tp")+'"></td>';
          ret += '</tr>';
        });
        ret += '</tbody></table>';
        ret += '</div>';
      }
    });
    $("#eleves").html(ret);
  }

  function recup_prof($groupe){
    // Renvoie le prof selectionné dans ce groupe
    return $groupe.find(".form-control  option:selected").attr("label");
  }

  function retrouve_creneau(id_groupe){
    // Renvoie l'id du creneau occupé par le groupe id_groupe en se basant sur shaker
    var id_creneau = "";
    $.each(shaker, function(cre, obj) {
      if (obj.groupe == id_groupe){
        id_creneau = cre;
        return false; // Interrompt la boucle dès que la valeur est trouvée
      }
    });
    return id_creneau;
  }

  function mise_a_jour_prof(id_groupe, choix_prof){
    // Met à jour le shaker lors du choix d'un prof dans une des listes déroulantes
    var id_creneau = retrouve_creneau(id_groupe);
    // Retrouve le crénau du groupe et écrit le nouveau prof
    shaker[id_creneau].prof = choix_prof;
    
    // Vérifie la cohérence (pas 2 fois le même prof dans le même créneau)
    verifie_coherence_creneau(id_creneau);
  }

  function verifie_coherence_creneau(id_creneau){
    // Vérifie la cohérence du crenau horaire : pas 2 fois le même prof dans le même créneau  
    //Liste les id des droppable commençant comme id_creneau (mais sans la lettre de tp ni l'avant dernière) et les trie
    var listecre = $(".droppable").map(function(i,k){
      if (k.id.includes(id_creneau.split("__")[0])){
        $("#"+k.id).removeClass("profKO"); // On en profite pour virer la classe profKO de tous les créneaux concernés
        return k.id;
      }
    }).sort();
    // Parcours de tous les créneaux de la même tranche horaire
    var liste_prof = {}; // liste des groupes avec leur prof respectif (rien si vide)
    $.each(listecre, function(i, id_cre) {
      if (shaker[id_cre].prof != "(vide)"){
        liste_prof[shaker[id_cre].groupe] = shaker[id_cre].prof;
      }
    });
    // 2ème parcours avec cette fois mise en surbrillance des incohérences en s'appuyant sur liste_prof
    // seulement s'il y a au moins 2 profs choisis
    if (Object.keys(liste_prof).length >= 2){
      $.each(listecre, function(i, id_cre) {
        if (shaker[id_cre].prof != "(vide)"){
          $.each(liste_prof, function(id_group, le_prof) {
            if ((shaker[id_cre].groupe != id_group) && (shaker[id_cre].prof == le_prof)){
              $("#"+id_cre).addClass("profKO");
              return false;
            }
          });
        }
      });
    }
    
    // Vérification de la présence d'assez de prof pour potentielement remplir chaque créneau de tp
    // Parcours de tous les créneaux de la même tranche horaire
    var compte_profs = {}; // liste tous les profs possibles par créneau
    $.each(profs, function(i, prf){
      compte_profs[prf] = [];
    });
    $.each(listecre, function(i, id_cre) {
      $("#"+shaker[id_cre].groupe).find("select option").each(function(){
        if ($(this).attr("label") != "(vide)"){
          compte_profs[$(this).attr("label")].push(shaker[id_cre].groupe);
        }
      });
    });
    // Comptage de chaque prof possible dans chaque groupe et il doit y avoir globalement au moins 1 prof différent par TP et par créneau
    var id_panel = id_creneau.split("__")[0]; // se base sur la présence de "__" pour choper l'id du panel
    var longueur = $("#"+id_panel).find("td").length; // calcul du nb de tp par créneaux
    if (Object.keys(compte_profs).length < longueur){
      $("#"+id_panel).addClass("panel-danger");
      var message = $("#"+id_panel).data("nom") + " => seulement " + Object.keys(compte_profs).length + " prof(s) :";
      $.each(Object.keys(compte_profs), function(i,prof){
        message += " "+prof;
      });
      $("#"+id_panel).children("div").text(message);
    }else{
      $("#"+id_panel).removeClass("panel-danger");
      $("#"+id_panel).children("div").text($("#"+id_panel).data("nom"));
    }
    //~ var message = "";
    //~ $.each(Object.keys(compte_profs), function(i,prof){
      //~ if (compte_profs[prof].length == 0){
          //~ message += " "+prof;
      //~ }
    //~ });
    //~ if (message != ""){
        //~ $("#"+id_panel).addClass("panel-danger");
        //~ $("#"+id_panel).children("div").text($("#"+id_panel).data("nom") + " => il manque " + message + " !");
    //~ }else{
      //~ $("#"+id_panel).removeClass("panel-danger");
      //~ $("#"+id_panel).children("div").text($("#"+id_panel).data("nom"));
    //~ }
  }

  function affiche_recap_prof(){
    // Remplit le tableau récapitulatif des profs à partir de shaker
    // Initialisation de l'objet recap vide
    var recap = {};
    var cre = $.map(creneaux, function(value, key) {return key;}).sort(); // Liste des créneaux horaires
    $.each(profs, function(i, prf){
      recap[prf] = {};
      $.each(cre, function(j, val) {
        recap[prf][creer_id(val)] = [];
      });
    });
    
    // Remplissage du recap à partir du shaker
    $.each(shaker, function(creneau, obj) {
      if (obj.prof != "(vide)"){
        // {"prof01": {"créneau01": ["A","C"], ...}, ...}
        // se base sur la présence de "__" pour choper l'id du panel
        recap[obj.prof][creneau.split("__")[0]].push(creneau.split("__")[1]);
      }
    });
    
    // Remplissage du tableau
    // Le tableau est supposé déjà construit (par affiche_profs), on ne fait que remplir les cellules
    $.each(profs, function(i, prf){
      $.each(cre, function(j, val) {
        var id_val = creer_id(val);
        var id_cellule = prf + "__" + id_val;
        if (recap[prf][id_val].length > 0){
          if (recap[prf][id_val].length == 1){
            // 1 seul tp prévu pour ce prof à ce créneau horaire donc affichage joli
            var lettre_tp = recap[prf][id_val][0].split("_")[0];
            var salle_tp = recap[prf][id_val][0].split("_")[1];
            $("#"+id_cellule).html("<kbd>"+lettre_tp+"</kbd>&nbsp;<small>"+salle_tp+"</small>");
          }else{
            // plusieurs tp prévus pour ce prof à ce créneau horaire donc affichage méchant
            var les_tp = "";
            $.each(recap[prf][id_val], function(i, tp) {
              les_tp += " <code>"+tp.split("_")[0]+"</code>";
            });
            $("#"+id_cellule).html(les_tp);
          }
        }else{
          $("#"+id_cellule).html("");
        }
      });
    });
  }

  function affiche_recap_eleves(){
    // Remplit le tableau récapitulatif des élèves à partir de eleves_par_classe
    // Le tableau est supposé déjà construit (par affiche_eleves), on ne fait que remplir les cellules
    $.each(Object.keys(eleves_par_classe), function(i,classe){
      $.each(eleves_par_classe[classe], function(eleve, groupe) {
        var creneau_joli = Object.keys(creneaux).sort()[retrouve_creneau(groupe).substr(0,1)-1]; // Hack pourri pour avoir un créneau affichable joli
        $("#"+creer_id(eleve+"__cre")).html(creneau_joli.substr(2,creneau_joli.length));
        $("#"+creer_id(eleve+"__tp")).html($("#"+groupe).data("tp"));
      });
    });
  }

  function redistribue_groupes(placer_les_profs){
    // Redistribue tous les groupes à leur place en se basant sur shaker
    // Nécessaire lors d'un affichage/masquage des élèves et au chargement d'une sauvegarde
    // Suivant si "placer_les_profs" est true ou false, on remet les profs ou pas
    $.each(shaker, function(cre, obj) {
      if (placer_les_profs){
        // Sélection du prof enregistré dans shaker
        $("#"+obj.groupe+"-select option[label='"+ obj.prof +"']").prop('selected', true);
      }
      droppable_drop($("#"+cre), $("#"+obj.groupe));
      mise_a_jour_prof(obj.groupe, obj.prof);
    });
  }

  function affiche_imprimer(){
    // Affiche un récapitulatif par créneau des élèves et du prof surveillant
    var cre = $.map(creneaux, function(value, key) {return key;}).sort();
    var ret = '<div class="well">';
    $.each(cre, function(i, val) {
      var id_cre = creer_id(val);
      var bouton_tout_imprimer;
      if (i===0){   //Ajout du bouton pour tout imprimer seulement sur le 1er
        bouton_tout_imprimer = '<button class="btn btn-success btn-panel pull-right yo-print hidden-print" data-idtoprint="imprimer" data-titre="Emploi du temps" style="margin-right: 10px;">Tout Imprimer</button>';
      }else bouton_tout_imprimer = '';
      ret += '<div id="imp_'+id_cre+'" class="panel panel-primary panel-yo" data-nom="'+val.substr(2)+'"><div class="panel-heading print-titre">'+val.substr(2)+'<button class="btn btn-warning btn-panel pull-right yo-print hidden-print" data-idtoprint="imp_'+id_cre+'" data-titre="Emploi du temps">Imprimer</button>'+bouton_tout_imprimer+'</div>';
      ret += '<table class="table table-bordered yo-striped yo-recap print-cell-pointille"><tbody><tr>';
      $('#'+id_cre+' td').each(function(j, el){
        var prof = shaker[el.id].prof;
        var groupe_id = shaker[el.id].groupe;
        var le_groupe = $.grep(groupes, function(e){ return e.id == groupe_id; })[0]; // Retrouve un groupe dans "groupes" à partir de son id
        var tp = le_groupe.TP;
        var salle = $("#"+el.id).data("salle");
        var eleves = le_groupe.eleves;
        ret += '<td class=""><pre><kbd class="lead">'+tp+'</kbd><span class="print-moyen lead" style="font-weight:bold"> - <span class="surligne">'+prof+'</span>&nbsp;<small>'+salle+'</small></span></pre>';
        // ret += '<td class=""><kbd class="lead">'+tp+'</kbd><span class="print-moyen" style="font-weight:bold"> - <span class="surligne">'+prof+'</span>&nbsp;<small>'+salle+'</small></span><br />';
        $.each(eleves, function(i, nom) {
          ret += '<span class="print-petit">'+nom+'</span><br />';
        });
        ret += '</td>';
      });
      ret += '</tr></tbody></table>';
      ret += '</div>';
    });
    ret += '</div>';
    $("#imprimer").html(ret);
  }
  
  function affiche_imprimer2(){
    // Affiche un récapitulatif par prof des élèves et créneaux
    var recap = {};
    var cre = $.map(creneaux, function(value, key) {return key;}).sort(); // Liste des créneaux horaires
    $.each(profs, function(i, prf){
      recap[prf] = {};
      $.each(cre, function(j, val) {
        recap[prf][creer_id(val)] = [];
      });
    });
    
    // Remplissage du recap à partir du shaker
    $.each(shaker, function(creneau, obj) {
      if (obj.prof != "(vide)"){
        // {"prof01": {"créneau01": ["A","C"], ...}, ...}
        // se base sur la présence de "__" pour choper l'id du panel
        recap[obj.prof][creneau.split("__")[0]].push(creneau.split("__")[1]+"##"+obj.groupe); // => "C_135##grp-14"
      }
    });
    
    var ret = '<div class="well">';
    $.each(profs, function(i, prf){
      //var id_cre = creer_id(val);
      var bouton_tout_imprimer;
      if (i===0){   //Ajout du bouton pour tout imprimer seulement sur le 1er
        bouton_tout_imprimer = '<button class="btn btn-success btn-panel pull-right yo-print hidden-print" data-idtoprint="imprimer2" data-titre="Emploi du temps par prof" style="margin-right: 10px;">Tout Imprimer</button>';
      }else bouton_tout_imprimer = '';
      ret += '<div id="imp2_'+prf+'" class="panel panel-primary panel-yo" data-nom="'+prf+'"><div class="panel-heading print-titre">'+prf+'<button class="btn btn-warning btn-panel pull-right yo-print hidden-print" data-idtoprint="imp2_'+prf+'" data-titre="Emploi du temps par prof">Imprimer</button>'+bouton_tout_imprimer+'</div>';
      ret += '<table class="table table-bordered yo-striped yo-recap print-cell-pointille"><tbody><tr>';
      $.each(cre, function(j, val) {
        var id_val = creer_id(val);
        ret += '<td class=""><pre><span class="print-moyen lead" style="font-weight:bold">'+val.substr(2)+'&nbsp;';
        if (recap[prf][id_val].length > 0){
          if (recap[prf][id_val].length == 1){
            // 1 seul tp prévu pour ce prof à ce créneau horaire donc affichage joli
            var lettre_tp = recap[prf][id_val][0].split("##")[0].split("_")[0]; // => "C_135##grp-14"
            var salle_tp = recap[prf][id_val][0].split("##")[0].split("_")[1];
            ret += '<kbd class="lead">'+lettre_tp+'</kbd></span>&nbsp;<small>'+salle_tp+'</small></pre>';
            var le_groupe = $.grep(groupes, function(e){ return e.id == recap[prf][id_val][0].split("##")[1]; })[0];  // Retrouve un groupe dans "groupes" à partir de son id
            var eleves = le_groupe.eleves;
            $.each(eleves, function(i, nom) {
              ret += '<span class="print-petit">'+nom+'</span><br />';
            });
          }else{
            // plusieurs tp prévus pour ce prof à ce créneau horaire donc affichage méchant
            var les_tp = "";
            $.each(recap[prf][id_val], function(i, tp) {
              les_tp += " <code>"+tp.split("_")[0]+"</code>";
              //~ les_tp += ' <kbd class="bg-danger lead">'+tp.split("_")[0]+'</kbd>';
            });
            ret += '<span class="surligne">'+les_tp.substr(1)+'</span></pre>';
            ret += '<div class="alert alert-danger" role="alert">IMPOSSIBLE</div>';
          }
        }else{
          ret += '<span class="surligne">Rien</span></pre>';
        }
      });
      ret += '</td>';
      ret += '</tr></tbody></table>';
      ret += '</div>';
    });
    ret += '</div>';
    $("#imprimer2").html(ret);
  }
